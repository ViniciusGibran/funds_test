//
//  FundViewModel.swift
//  funds_tata_test
//
//  Created by Vinicius Gibran Gehlen Bornholdt on 06/06/18.
//  Copyright © 2018 Vinicius Gibran. All rights reserved.
//

import UIKit

class FundViewModel: NSObject {

    var fund: Fund!
    
    func getFund(completion: @escaping () -> Void) {
        FundRepository.getFund { [unowned self] fund in
            self.fund = fund
            completion()
        }
    }
}
