//
//  FundViewController.swift
//  funds_tata_test
//
//  Created by Vinicius Gibran Gehlen Bornholdt on 06/06/18.
//  Copyright © 2018 Vinicius Gibran. All rights reserved.
//

import UIKit

class FundViewController: UIViewController {

    //MARK: Components
    private var tableView: UITableView!
    
    //MARK: Variables
    internal var viewModel: FundViewModel = FundViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = matDarkColor
        addTableView()
        tableView.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        view.activityStart()
        viewModel.getFund { [unowned self] in
            self.tableView.reloadData()
            self.tableView.isHidden = false
            self.view.activityStop()
        }
    }
    
    //MARK: Helpers
    func addTableView() {
        let viewRect = CGRect(x: 10, y: 40, width: UIScreen.main.bounds.width - 20, height: UIScreen.main.bounds.height - 60)
        let contentView = UIView(frame: viewRect)
        contentView.layer.cornerRadius = 5
        contentView.backgroundColor = matColor
        contentView.clipsToBounds = true
        
        let tableRect = CGRect(x: 5, y: 5, width: UIScreen.main.bounds.width - 30, height: UIScreen.main.bounds.height - 70)
        tableView = UITableView(frame: tableRect)
        tableView.dataSource = self
        tableView.delegate = self
        tableView.separatorStyle = .none
        tableView.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0)
        tableView.backgroundColor = .clear
        tableView.showsVerticalScrollIndicator = false
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 200
        tableView.registerFundCells()
        contentView.addSubview(tableView)
        view.addSubview(contentView)
    }
}

//MARK: TableViewDataSource | TableViewDelegate
extension FundViewController: UITableViewDataSource, UITableViewDelegate {

    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1
        case 1:
            guard let fund = viewModel.fund else {return 0}
            return fund.info.count + fund.downInfo.count
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let fundCell = tableView.dequeueReusableCell(withIdentifier: "FundTableViewCell") as! FundTableViewCell
            return fundCell
        case 1:
            let fundInfoCell = tableView.dequeueReusableCell(withIdentifier: "FundInfoTableViewCell") as! FundInfoTableViewCell
            return fundInfoCell
        default:
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let fund = viewModel.fund else {return}
        if let fundCell = cell as? FundTableViewCell {
            fundCell.titleLabel.text = fund.title
            fundCell.fundNameLabel.text = fund.fundName
            fundCell.whatIsLabel.text = fund.whatIs
            fundCell.definitionLabel.text = fund.definition
            fundCell.riskTitleLabel.text = fund.riskTitle + ":"
            fundCell.riskLabel.text = String(fund.risk)
            fundCell.twelveMonthsLabel.text = "12 meses:"
            fundCell.twelveMonthsCdiFundLabel.text = "CDI: \(fund.moreInfo.twelveMonths.cdi) - Fundo: \(fund.moreInfo.twelveMonths.fund)"
            fundCell.monthLabel.text = "Mês:"
            fundCell.monthCdiFundLabel.text = "CDI: \(fund.moreInfo.month.cdi) - Fundo: \(fund.moreInfo.month.fund)"
            fundCell.yearLabel.text = "Ano:"
            fundCell.yearCdiFundLabel.text = "CDI: \(fund.moreInfo.year.cdi) - Fundo: \(fund.moreInfo.year.fund)"
            fundCell.infoTitleLabel.text = fund.infoTitle + ":"
            
            fundCell.riskLabel.textColor = matOrangeColor
            fundCell.twelveMonthsCdiFundLabel.textColor = matOrangeColor
            fundCell.monthCdiFundLabel.textColor = matOrangeColor
            fundCell.yearCdiFundLabel.textColor = matOrangeColor
            fundCell.titleLabel.textColor = matOrangeColor
        }
        
        
        if let fundInfoCell = cell as? FundInfoTableViewCell {
            if fund.info.count - 1 >= indexPath.row  {
                let info = fund.info[indexPath.row]
                fundInfoCell.infoLabel.attributedText = setInfoLabelWith(whiteText: info.name!, coloredText: info.data!)
                return
            }
         
            if fund.downInfo.count - 1 >= indexPath.row - (fund.info.count) {
                let downInfo = fund.downInfo[indexPath.row - (fund.info.count)]
                fundInfoCell.infoLabel.attributedText = setInfoLabelWith(whiteText: downInfo.name!, coloredText: downInfo.data ?? "-")
            }
        }
    }
    
    private func setInfoLabelWith(whiteText: String, coloredText: String) -> NSMutableAttributedString {
        
        let finalText = "\(whiteText): \(coloredText)"
        
        let range = (finalText as NSString).range(of: coloredText, options: .caseInsensitive)
        
        let coloredAttrs: [String:Any] = [
            NSFontAttributeName: UIFont.systemFont(ofSize: 15.0),
            NSForegroundColorAttributeName: matOrangeColor
        ]
        
        let whiteAttrs: [String:Any] = [
            NSFontAttributeName: UIFont.systemFont(ofSize: 15.0),
            NSForegroundColorAttributeName: UIColor.white
        ]
        
        let attResult =  NSMutableAttributedString(string: finalText, attributes: whiteAttrs)
        attResult.setAttributes(coloredAttrs, range: range)
        
        return attResult
        
    }
}



