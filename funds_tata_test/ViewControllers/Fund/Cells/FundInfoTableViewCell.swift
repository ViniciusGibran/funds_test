//
//  FundInfoTableViewCell.swift
//  funds_tata_test
//
//  Created by Vinicius Gibran Gehlen Bornholdt on 07/06/18.
//  Copyright © 2018 Vinicius Gibran. All rights reserved.
//

import UIKit

class FundInfoTableViewCell: UITableViewCell {
    @IBOutlet weak var infoLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        backgroundColor = .clear //use only in small lists
    }
}
