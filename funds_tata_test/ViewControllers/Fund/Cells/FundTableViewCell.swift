//
//  FundTableViewCell.swift
//  funds_tata_test
//
//  Created by Vinicius Gibran Gehlen Bornholdt on 06/06/18.
//  Copyright © 2018 Vinicius Gibran. All rights reserved.
//

import UIKit

class FundTableViewCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var fundNameLabel: UILabel!
    @IBOutlet weak var whatIsLabel: UILabel!
    @IBOutlet weak var definitionLabel: UILabel!
    @IBOutlet weak var riskTitleLabel: UILabel!
    @IBOutlet weak var riskLabel: UILabel!
    @IBOutlet weak var twelveMonthsLabel: UILabel!
    @IBOutlet weak var twelveMonthsCdiFundLabel: UILabel!
    @IBOutlet weak var monthLabel: UILabel!
    @IBOutlet weak var monthCdiFundLabel: UILabel!
    @IBOutlet weak var yearLabel: UILabel!
    @IBOutlet weak var yearCdiFundLabel: UILabel!
    @IBOutlet weak var infoTitleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        backgroundColor = .clear //use only in small lists
    }
    
}
