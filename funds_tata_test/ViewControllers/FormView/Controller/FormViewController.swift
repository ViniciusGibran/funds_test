//
//  FormViewViewController.swift
//  funds_tata_test
//
//  Created by Vinicius Gibran on 03/06/18.
//  Copyright © 2018 Vinicius Gibran. All rights reserved.
//

import UIKit

class FormViewController: UIViewController {
    
    //MARK: Components
    @IBOutlet weak var fundsLabel: UILabel!
    internal var tableView: UITableView!
    
    //MARK: Variables
    internal var viewModel: FormViewModel = FormViewModel()
    internal var formValidation: (name: Bool, telNumber: Bool, email: Bool) = (false, false, false)
    internal var cellSendIndex: IndexPath!

    override func viewDidLoad() {
        super.viewDidLoad()
        addTableView()
        
        if let navigation = navigationController?.navigationBar {
            navigation.setBackgroundImage(UIImage(), for: .default)
            navigation.shadowImage = UIImage()
            navigation.isTranslucent = true
        }
        
        registerKeyboardNotificationsFor(scrollView: tableView)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        view.activityStart()
        viewModel.getFormCells { [unowned self] success  in
            if success {
                self.fundsLabel.isHidden = true
                self.tableView.reloadData()
            }else {
                self.tableView.isHidden = true
                self.fundsLabel.text = "😵 \nOpa, aconteceu algo de errado.\n Tente mais tarde!"
            }
            self.view.activityStop()
        }
    }
    
    //MARK: Helpers
    func addTableView() {
        let rect = CGRect(x: 0, y: 40, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        tableView = UITableView(frame: rect)
        tableView.dataSource = self
        tableView.rowHeight = 80
        tableView.separatorStyle = .none
        tableView.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0)
        tableView.backgroundColor = .clear
        tableView.showsVerticalScrollIndicator = false
        tableView.registerFormCells()
        view.addSubview(tableView)
    }
}

//MARK: TableViewDataSource
extension FormViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.formCells.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard !viewModel.formCells.isEmpty else {return UITableViewCell()}
        
        let formCell = viewModel.formCells[indexPath.row]
        
        switch formCell.type {
        case .text:
            let textCell = tableView.dequeueReusableCell(withIdentifier: "FormViewTextCell") as! FormViewTextCell
            textCell.messageLabel?.text = formCell.message
            textCell.formType = formCell.type
            textCell.formTypeField = formCell.typeField
            return textCell
            
        case .field:
            let fieldCell = tableView.dequeueReusableCell(withIdentifier: "FormViewFieldCell") as! FormViewFieldCell
            fieldCell.messageLabel?.text = formCell.message
            fieldCell.formType = formCell.type
            fieldCell.formTypeField = formCell.typeField
            fieldCell.delegate = self
            return fieldCell
            
        case .checkbox:
            let checkBoxCell = tableView.dequeueReusableCell(withIdentifier: "FormViewCheckBoxCell") as! FormViewCheckBoxCell
            checkBoxCell.messageLabel?.text = formCell.message
            checkBoxCell.formType = formCell.type
            checkBoxCell.formTypeField = formCell.typeField
            return checkBoxCell
            
        case .send:
            let sendCell = tableView.dequeueReusableCell(withIdentifier: "FormViewSendCell") as! FormViewSendCell
            sendCell.delegate = self
            cellSendIndex = indexPath
            if isFormValid() {sendCell.enableSenderButton()}
            return sendCell
        default:
            return UITableViewCell()
        }
    }
}

//MARK: FormViewSendDelegate
extension FormViewController: FormViewDelegate {
    func verifyFormInputs(formTypeField: FormTypeField, isValid: Bool) {
        switch formTypeField {
        case .email:
            formValidation.email = isValid
        case .text:
            formValidation.name = isValid
        case .telNumber:
            formValidation.telNumber = isValid
        default:
            break
        }
        
        if isFormValid() {
            if let cell = self.tableView.cellForRow(at: cellSendIndex) as? FormViewSendCell {
               cell.enableSenderButton()
            }
        }
    }
    
    func didSelectSendAction() {
        let fundVC = FundViewController()
        present(fundVC, animated: true, completion: nil)
    }
    
    func isFormValid() -> Bool {
        return formValidation.email && formValidation.name && formValidation.telNumber
    }
}





