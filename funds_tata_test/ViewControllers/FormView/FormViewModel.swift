//
//  FormViewModel.swift
//  funds_tata_test
//
//  Created by Vinicius Gibran on 03/06/18.
//  Copyright © 2018 Vinicius Gibran. All rights reserved.
//

import UIKit

class FormViewModel {
    
    var formCells: [FormCell] = []
    
    func getFormCells(completion: @escaping(_ success: Bool) -> Void) {
        FormCellRepository.getFormCells { [unowned self] formCells in
            self.formCells = formCells
            completion(!formCells.isEmpty)
        }
    }
}
