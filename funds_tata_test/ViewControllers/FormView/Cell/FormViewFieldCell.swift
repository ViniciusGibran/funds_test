//
//  FormViewFieldCell.swift
//  funds_tata_test
//
//  Created by Vinicius Gibran on 03/06/18.
//  Copyright © 2018 Vinicius Gibran. All rights reserved.
//

import UIKit

class FormViewFieldCell: FormViewBaseCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        inputTextField.delegate = self
        inputTextField.setPadding(value: 5)
    }
}

//MARK: TextField Delegate
extension FormViewFieldCell: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        verifyFormInputs()
        guard string != "" else {return true}
        
        switch formTypeField {
        case .telNumber:
            var tfInput = textField.text!
            let totalChar = textField.text!.characters.count
            
            if totalChar >= 14 {return false}
            
            if totalChar == 2 {
                tfInput.insert("(", at: tfInput.index(tfInput.startIndex, offsetBy: 0))
                tfInput.insert(")", at: tfInput.index(tfInput.startIndex, offsetBy: 3))
                tfInput.insert(" ", at: tfInput.index(tfInput.startIndex, offsetBy: 4))
            }
            
            if totalChar == 9 {
                tfInput.insert(" ", at: tfInput.index(tfInput.startIndex, offsetBy: 9))
            }
            
            textField.text! = tfInput
            return true
        case .email:
            let tfInput = textField.text!
            if tfInput.contains("@") {
                if tfInput.contains("."){
                    textField.textColor = isValidEmail(tfInput) ? .black : .red
                }
            }
            return true
            
        default:
            return true
        }
    }
}
