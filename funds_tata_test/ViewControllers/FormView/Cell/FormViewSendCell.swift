//
//  FormViewSendCell.swift
//  funds_tata_test
//
//  Created by Vinicius Gibran on 03/06/18.
//  Copyright © 2018 Vinicius Gibran. All rights reserved.
//

import UIKit


class FormViewSendCell: FormViewBaseCell {
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        sendButton.alpha = 0.5
        sendButton.isUserInteractionEnabled = false
    }
    
    
    @IBAction func senderButtonAction(_ sender: UIButton) {
        debugPrint("Sender Button Action Taped")
        delegate?.didSelectSendAction()
    }
    
    func enableSenderButton() {
        sendButton.alpha = 1.0
        sendButton.isUserInteractionEnabled = true
    }
    
}
