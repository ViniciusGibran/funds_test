//
//  FormViewBaseCell.swift
//  funds_tata_test
//
//  Created by Vinicius Gibran on 03/06/18.
//  Copyright © 2018 Vinicius Gibran. All rights reserved.
//

import UIKit

protocol FormViewDelegate: class {
    func didSelectSendAction()
    func verifyFormInputs(formTypeField: FormTypeField, isValid: Bool)
}


class FormViewBaseCell: UITableViewCell {

    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var inputTextField: UITextField!
    @IBOutlet weak var checkBoxSwitcher: UISwitch!
    @IBOutlet weak var sendButton: UIButton!
    
    var formType: FormType = .none
    var formTypeField: FormTypeField = .none {
        didSet{
            guard inputTextField != nil else {return}
            switch formTypeField {
            case .telNumber:
                inputTextField.keyboardType = .phonePad
            case .email:
                inputTextField.keyboardType = .emailAddress
            case .text:
                inputTextField.autocapitalizationType = .sentences
            default:
                break
            }
        }
    }
    
     weak var delegate: FormViewDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        backgroundColor = .clear //use only in small lists
    }
    
    internal func verifyFormInputs() {
        let input = inputTextField.text!
        switch formTypeField {
        case .email:
            delegate?.verifyFormInputs(formTypeField: .email, isValid: isValidEmail(input))
        case .text:
            delegate?.verifyFormInputs(formTypeField: .text, isValid: input.count >= 5)
        case .telNumber:
            delegate?.verifyFormInputs(formTypeField: .telNumber, isValid: input.count >= 14)
        default:
            break
        }
    }
    
    internal func isValidEmail(_ input: String) -> Bool {
        let predicate = NSPredicate(format: "SELF MATCHES %@", "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,2}")
        return predicate.evaluate(with: input)
    }

}
