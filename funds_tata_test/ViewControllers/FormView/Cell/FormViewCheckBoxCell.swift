//
//  FormViewCheckBoxCell.swift
//  funds_tata_test
//
//  Created by Vinicius Gibran on 03/06/18.
//  Copyright © 2018 Vinicius Gibran. All rights reserved.
//

import UIKit

class FormViewCheckBoxCell: FormViewBaseCell {

    override func awakeFromNib() {
        super.awakeFromNib()
     
    }
    
    @IBAction func checkBoxAction(_ sender: UISwitch) {
        debugPrint("Swticher Status Changed:", sender.isOn)
    }
    
    
}
