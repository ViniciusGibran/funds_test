//
//  TableView.swift
//  funds_tata_test
//
//  Created by Vinicius Gibran Gehlen Bornholdt on 06/06/18.
//  Copyright © 2018 Vinicius Gibran. All rights reserved.
//

import UIKit

extension UITableView {

    public func registerFormCells() {
        self.register(UINib(nibName: "FormViewFieldCell", bundle: nil), forCellReuseIdentifier: "FormViewFieldCell")
        self.register(UINib(nibName: "FormViewTextCell", bundle: nil), forCellReuseIdentifier: "FormViewTextCell")
        self.register(UINib(nibName: "FormViewCheckBoxCell", bundle: nil), forCellReuseIdentifier: "FormViewCheckBoxCell")
        self.register(UINib(nibName: "FormViewSendCell", bundle: nil), forCellReuseIdentifier: "FormViewSendCell")
        self.register(FormViewBaseCell.self, forCellReuseIdentifier: "FormViewBaseCell")
    }
    
    public func registerFundCells() {
        self.register(UINib(nibName: "FundTableViewCell", bundle: nil), forCellReuseIdentifier: "FundTableViewCell")
        self.register(UINib(nibName: "FundInfoTableViewCell", bundle: nil), forCellReuseIdentifier: "FundInfoTableViewCell")
    }
}
