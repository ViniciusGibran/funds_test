//
//  ViewController.swift
//  funds_tata_test
//
//  Created by Vinicius Gibran Gehlen Bornholdt on 06/06/18.
//  Copyright © 2018 Vinicius Gibran. All rights reserved.
//

import UIKit

extension UIViewController {
    
    open var matColor: UIColor {
        get {
            return UIColor.colorWithRed(red: 242, green: 242, blue: 242, andAlpha: 0.2)
        }
    }
    
    open var matOrangeColor: UIColor {
        get{
            return UIColor.colorWithRed(red: 242, green: 125, blue: 0, andAlpha: 1)
        }
    }
    
    open var matDarkColor: UIColor {
        get{
            return UIColor.colorWithRed(red: 46, green: 58, blue: 78, andAlpha: 1)
        }
    }
    
    private func setScrollView(scrollView: UIScrollView) {
        scrollView.tag = 111
    }
    
    private func getScrollView() -> UIScrollView? {
        return self.view.viewWithTag(111) as? UIScrollView
    }
    
    /**
     Registra as notifições de abertura e fechamento do teclado.
     
     As ações de abrir e fechar o teclado estarão associadas aos métodos **keyboardDidShow** e **keyboardDidHide**, respectivamente.
     */
    internal func registerKeyboardNotificationsFor(scrollView: UIScrollView!) {
        setScrollView(scrollView: scrollView)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardDidShow(_:)), name: NSNotification.Name(rawValue: NSNotification.Name.UIKeyboardWillShow.rawValue), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardDidHide(_:)), name: NSNotification.Name(rawValue: NSNotification.Name.UIKeyboardWillHide.rawValue), object: nil)
        
        addTapRecognizerToSubview()
    }
    
    /**
     Método disparado ao abrir o teclado. Por padrão, não há implementação neste método.
     
     - important: Este método não será chamado caso o método **registerKeyboardNotifications** não tenha sido chamado anteriormente.
     Os view controllers que desejarem obter informações do teclado devem sobrescrever este método.
     
     - parameter notification: A notificação com as informações do teclado como tempo de duração da animação, tamanho, etc.
     */
    internal func keyboardDidShow(_ notification: Foundation.Notification) {
        
        guard let userInfo = (notification as NSNotification).userInfo else {
            return
        }
        
        guard let keyboardSize = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else {
            return
        }
        
        if let _scrollView = getScrollView() {
            //rect
            let keyboardRect = view.convert(keyboardSize, from: nil)
            //pega a posição do scroll
            let _scrollViewRect: CGRect = view.convert(_scrollView.frame, from: _scrollView.superview)
            //acerta o tamanho exato necessário
            let hiddenScrollViewRect: CGFloat = _scrollViewRect.intersection(keyboardRect).height + 50.0
            
            if _scrollViewRect.intersects(keyboardRect){
                let contentInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: hiddenScrollViewRect, right: 0.0)
                _scrollView.contentInset = contentInsets
                _scrollView.scrollIndicatorInsets = contentInsets
            }
        }
    }
    
    /**
     Método disparado ao fechar o teclado. Por padrão, não há implementação neste método.
     
     - important: Este método não será chamado caso o método **registerKeyboardNotifications** não tenha sido chamado anteriormente.
     Os view controllers que desejarem obter informações do teclado devem sobrescrever este método.
     
     - parameter notification: A notificação com as informações do teclado como tempo de duração da animação, tamanho, etc.
     */
    internal func keyboardDidHide(_ notification: Notification) {
        if let _scrollView = getScrollView() {
            //reset scrollview
            let contentInsets = UIEdgeInsets.zero
            _scrollView.contentInset = contentInsets
            _scrollView.scrollIndicatorInsets = contentInsets
        }
    }
    
    ///Add Tap Recognizer
    internal func addTapRecognizerToSubview() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(endEditingOnTap))
        tap.numberOfTapsRequired = 1
        tap.numberOfTouchesRequired = 1
        
        view.addGestureRecognizer(tap)
    }
    
    dynamic private func endEditingOnTap() {
        view.endEditing(true)
    }
}

