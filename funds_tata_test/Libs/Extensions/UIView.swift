//
//  UIView.swift
//  funds_tata_test
//
//  Created by Vinicius Gibran Gehlen Bornholdt on 06/06/18.
//  Copyright © 2018 Vinicius Gibran. All rights reserved.
//

import UIKit

extension UIView {

    func activityStart() {
        let actv = UIActivityIndicatorView(activityIndicatorStyle: .white)
        actv.center = CGPoint(x: self.bounds.width/2, y: self.bounds.height/2)
        self.insertSubview(actv, at: 1)
        actv.startAnimating()
    }
    
    func activityStop() {
        for view in self.subviews {
            if let _ = view as? UIActivityIndicatorView {
                view.removeFromSuperview()
            }
        }
    }
}
