//
//  AppDelegate.swift
//  funds_tata_test
//
//  Created by Vinicius Gibran on 02/06/18.
//  Copyright © 2018 Vinicius Gibran. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        //custom window for this app
        let initialVC = FormViewController(nibName: "FormViewController", bundle: nil)
        window = createMainWindow(initialVC)
    
        return true
    }
    
    
    /**
     Create an `UIWindow` with initial `UINavigationController` and UIViewController`.
     - returns: `UIWindow` set and up for this app.
     */
    func createMainWindow(_ viewController: UIViewController) -> UIWindow {
        let navigationController = UINavigationController(rootViewController: viewController)
        
        //custom window for this app
        navigationController.navigationBar.tintColor = .white
        
        let window = UIWindow(frame: UIScreen.main.bounds)
        window.rootViewController = navigationController
        window.clipsToBounds = true
        
        window.makeKeyAndVisible()
        
        return window
    }
    
    
    func applicationWillResignActive(_ application: UIApplication) {}
    func applicationDidEnterBackground(_ application: UIApplication) {}
    func applicationWillEnterForeground(_ application: UIApplication) {}
    func applicationDidBecomeActive(_ application: UIApplication) {}
    func applicationWillTerminate(_ application: UIApplication) {}

}
