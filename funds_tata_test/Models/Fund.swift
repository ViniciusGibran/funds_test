//
//  Fund.swift
//  funds_tata_test
//
//  Created by Vinicius Gibran on 03/06/18.
//  Copyright © 2018 Vinicius Gibran. All rights reserved.
//

import UIKit

class Fund: Mappable {
    
    //MAKR: Helpers Structs
    internal struct DownInfo {
        var data: String?
        var name: String!
    }
    
    internal struct Info {
        var data: String!
        var name: String!
    }
    
    internal struct MoreInfo {
        var twelveMonths: (cdi: Double, fund: Double) = (0.0,0.0)
        var month: (cdi: Double, fund: Double) = (0.0,0.0)
        var year: (cdi: Double, fund: Double) = (0.0,0.0)
    }
    
    
    //MARK: Open Var
    open var definition: String!
    open var fundName: String!
    open var infoTitle: String!
    open var risk: Int!
    open var riskTitle: String!
    open var title: String!
    open var whatIs: String!
    open var downInfo: [DownInfo] = []
    open var info: [Info] = []
    open var moreInfo: MoreInfo = MoreInfo()
    
    required init?(map: Map) {}
    
    
    //MARK: Parse
    func mapping(map: Map) {
        definition <- map["definition"]
        fundName <- map["fundName"]
        infoTitle <- map["infoTitle"]
        risk <- map["risk"]
        riskTitle <- map["riskTitle"]
        title <- map["title"]
        whatIs <- map["whatIs"]
        
        if let downInfoList = map.JSON["downInfo"] as? NSArray {
            for downInfo in downInfoList {
                var downInfoObj = DownInfo()
                if let json = downInfo as? [String: Any] {
                    downInfoObj.data = json["data"] as? String
                    downInfoObj.name = json["name"] as? String
                    
                    self.downInfo.append(downInfoObj)
                }
            }
        }
        
        if let infoList = map.JSON["info"] as? NSArray {
            for info in infoList {
                var infoObj = Info()
                if let json = info as? [String: Any] {
                    infoObj.data = json["data"] as? String
                    infoObj.name = json["name"] as? String
                    
                    self.info.append(infoObj)
                }
            }
        }
        
        if let moreInfoJSON = map.JSON["moreInfo"] as? [String: AnyObject] {
            
            if let twelveMonthsJSON = moreInfoJSON["12months"] as? [String: Any] {
                if let cdi = twelveMonthsJSON["CDI"] as? Double, let fund = twelveMonthsJSON["fund"] as? Double {
                    self.moreInfo.twelveMonths.cdi = cdi
                    self.moreInfo.twelveMonths.fund = fund
                }
            }
            
            if let monthJSON = moreInfoJSON["month"] as? [String: Any] {
                if let cdi = monthJSON["CDI"] as? Double, let fund = monthJSON["fund"] as? Double {
                    self.moreInfo.year.cdi = cdi
                    self.moreInfo.year.fund = fund
                }
            }
            
            if let yearJSON = moreInfoJSON["year"] as? [String: Any] {
                if let cdi = yearJSON["CDI"] as? Double, let fund = yearJSON["fund"] as? Double {
                    self.moreInfo.year.cdi = cdi
                    self.moreInfo.year.fund = fund
                }
            }
        }
    }
}
