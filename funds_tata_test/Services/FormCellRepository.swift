//
//  FormCellRepository.swift
//  funds_tata_test
//
//  Created by Vinicius Gibran on 03/06/18.
//  Copyright © 2018 Vinicius Gibran. All rights reserved.
//

import UIKit
import Foundation


struct FormCell {
    var id: Int!
    var isHidden: Bool!
    var isRequired: Bool!
    var message: String!
    var show:Int!
    var topSpacing: Int!
    var type: FormType = .none
    var typeField: FormTypeField = .none
}

enum FormType: Int {
    case none, field, text, image, checkbox, send
}

enum FormTypeField: Int {
    case none, text, telNumber, email
}

class FormCellRepository: NSObject {
    
    
    static func getFormCells(completion: @escaping(_ formCells: [FormCell]) -> Void) {
        var returnList: [FormCell] = []
        if  let httpRequest = Foundation.URL(string: "https://floating-mountain-50292.herokuapp.com/cells.json") {
            if let data = try? Data(contentsOf: httpRequest) {
                guard let json = try? JSONSerialization.jsonObject(with: data, options: .allowFragments) else {
                    return
                    
                }
                guard let dict = json as? NSDictionary else {
                    return
                }
                
                if let cellsArray = dict["cells"] as? NSArray {
                    
                    var formCellObj = FormCell()
                    for formCellDict in cellsArray {
                        if let formCellJSON = formCellDict as? [String: Any] {
                            formCellObj.id = formCellJSON["id"] as? Int ?? 0
                            formCellObj.isHidden = formCellJSON["hidden"] as? Bool ?? false
                            formCellObj.isRequired = formCellJSON["required"] as? Bool ?? false
                            formCellObj.message = formCellJSON["message"] as? String ?? ""
                            formCellObj.show = formCellJSON["show"] as? Int ?? 0
                            formCellObj.topSpacing = formCellJSON["topSpacing"] as? Int ?? 0
                            if let type = formCellJSON["type"] as? Int {
                                formCellObj.type = FormType(rawValue: type)!
                            }
                            
                            if let typeField = formCellJSON["typefield"] as? Int {
                                formCellObj.typeField = FormTypeField(rawValue: typeField)!
                            }
                            
                            //DevTeam: erro api
                            if let typeFieldString = formCellJSON["typefield"] as? String {
                                if typeFieldString == "telnumber" {
                                    formCellObj.typeField = .telNumber
                                }
                            }
                            
                            returnList.append(formCellObj)
                        }
                        
                    }
                }
                
                completion(returnList)
                
            }
        }
    }
    
}
