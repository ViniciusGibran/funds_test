//
//  FundRepository.swift
//  funds_tata_test
//
//  Created by Vinicius Gibran on 03/06/18.
//  Copyright © 2018 Vinicius Gibran. All rights reserved.
//

import UIKit

class FundRepository: NSObject {

    static func getFund(completion: @escaping (_ fund: Fund) -> Void) {
        
        if  let httpRequest = Foundation.URL(string: "https://floating-mountain-50292.herokuapp.com/fund.json") {
            if let data = try? Data(contentsOf: httpRequest) {
                guard let json = try? JSONSerialization.jsonObject(with: data, options: .allowFragments) else {
                    return
                    
                }
                guard let dict = json as? NSDictionary else {
                    return
                }
                
                if let fundJSON = dict["screen"] as? [String: Any] {
                    if let fund = Fund(JSON: fundJSON) {
                        completion(fund)
                    }
                }
            }
        }
    }
}
